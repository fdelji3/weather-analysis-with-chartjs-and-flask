import csv 
import codecs
import requests
import sys
import pandas as pd
import matplotlib.pyplot as plt


def main_function():
    pandas_csv = pd.read_csv("time_data.csv")
    print(pandas_csv['TM'])
    x = pandas_csv['Año']
    y = pandas_csv['TM']
    # plot
    plt.plot(x,y)
    # beautify the x-labels
    plt.gcf().autofmt_xdate()
    plt.show()

    # ... process CSV file contents here ...
if __name__ == "__main__":
    main_function()