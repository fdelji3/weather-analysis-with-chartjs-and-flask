import flask
from flask import render_template, redirect, url_for,request,jsonify
import os
import pandas as pd


app = flask.Flask(__name__, static_url_path='/static',
            static_folder='static',
            template_folder='template')
app.config["DEBUG"] = True


@app.route("/home",methods=['GET','POST'])
def home():
    documents=os.listdir("./")
    doc_list = []
    for doc in documents:
        if doc.endswith(".csv"):
            doc_list.append(doc.replace(".csv",""))
    print(doc_list)
    return render_template("home.html",cities=doc_list)

@app.route("/get_data",methods=['GET', 'POST'])
def get_data():
    city = request.args.get('city')
    try:
        pandas_csv = pd.read_csv(city+".csv")
        x = pandas_csv['Año'].tolist()
        y = pandas_csv['TM'].tolist()
        w = pandas_csv['Tm'].tolist()
        z = pandas_csv['T'].tolist()
        lluvias = pandas_csv['PP'].tolist()
        data = [
        y,x,w,z,lluvias
        ]
    except:
        data = None
    return jsonify(data)
    
@app.route("/admin")
def admin():
    return redirect(url_for("home"))


if __name__ == '__main__':
    app.run(host="localhost", port=8000, debug=True)